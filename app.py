from flask import Flask, url_for, render_template
from pymongo import MongoClient
import os

app = Flask(__name__)

client = MongoClient(os.environ['MONGODB_HOST'], 27017)

# select a database
db = client.ltucareers

@app.route('/')
def home():
    """ route for the home view
    """
    
    return render_template("index.html", appName = "LTU Careers",)

@app.route('/profile')
def profile():
    """ route for the profile view
    """
    return render_template("my-profile.html", appName = "LTU Careers")

@app.route('/browse')
def browse():
    """ route for the listings view
    """
    jobs = db.jobs.find()
    print(jobs)
    return render_template("browse.html", appName = "LTU Careers", data=jobs)




app.run(host='0.0.0.0', debug=True)
